package com.indiant.android_indiant;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.indiant.ws.gestionMantenimiento.GestionMantenimiento;
import com.indiant.ws.gestionMantenimiento.model.Pais;
import com.indiant.ws.gestionMantenimiento.model.ReturnValue;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new EndpointsAsyncTask1().execute(this);
    }

    class EndpointsAsyncTask1 extends AsyncTask<Context, Void,ReturnValue> {
        private GestionMantenimiento myApiService = null;
        private Context context;

        @Override
        protected ReturnValue doInBackground(Context... params) {
            if(myApiService == null) {  // Only do this once
                GestionMantenimiento.Builder builder = new GestionMantenimiento.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), null)
                        .setRootUrl("http://10.0.2.2:8888/_ah/api/")//COMENTAR LINEA SI TU BACKEND ESTA DEPLOYADO
                        .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                            @Override
                            public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                abstractGoogleClientRequest.setDisableGZipContent(true);
                            }
                        });
                myApiService = builder.build();
            }
            context = params[0];
            try {
                Pais bean=new Pais();
                bean.setIdPais("co");
                bean.setNombre("colombia");
                ReturnValue values=myApiService.insertarPais(bean).execute();
                return values;
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(ReturnValue result) {
                Toast.makeText(context,result.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
